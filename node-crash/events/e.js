const fns = require('date-fns');
const numToString = num => num.toString();
const y = z => parseInt(z, 2);
const regex = /^\s*[\d]{4}\s+[q]{1}[1-4]{1}\s*$/mig;;
const testCases = ['2017 Q1', '2017 Q4', '2017 Q3', '2017 Q1Q Q2', '2017 Q2', '2020 q2', '2172 q2', 'xxx2 q2', '20172 Q1e', '20 Q1', 'xxxx Q4', '20166 Q3'];
console.log(parseInt('11110000', 2)); //240
console.log(y('00001001')); //9
testCases.filter(x => x.match(regex)).map(x => x.split(/\s+/)).forEach(x => console.log(x));
const dateString1 = '2017 Q1';
const dateString2 = '2017 Q2';
const dateString3 = '2017 Q3';
const dateString4 = '2017 Q4';
const date1 = fns.parse(dateString1, 'yyyy QQQQ', new Date(), { useAdditionalDayOfYearTokens: true, useAdditionalWeekYearTokens: true, });
const date2 = fns.parse(dateString2, 'yyyy QQQQ', new Date(), { useAdditionalDayOfYearTokens: true, useAdditionalWeekYearTokens: true, });
const date3 = fns.parse(dateString3, 'yyyy QQQQ', new Date(), { useAdditionalDayOfYearTokens: true, useAdditionalWeekYearTokens: true, });
const date4 = fns.parse(dateString4, 'yyyy QQQQ', new Date(), { useAdditionalDayOfYearTokens: true, useAdditionalWeekYearTokens: true, });
function getUTCDate(date = new Date()) { return new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000); }
console.log('quarter date =>', fns.format(date1, 'yyyy-MM-dd'));
console.log('quarter date =>', fns.format(date2, 'yyyy-MM-dd'));
console.log('quarter date =>', fns.format(date3, 'yyyy-MM-dd'));
console.log('quarter date =>', fns.format(date4, 'yyyy-MM-dd'));
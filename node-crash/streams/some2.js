const fs = require('fs');

const readableStream = fs.createReadStream('large-file.txt');
const writableStream = fs.createWriteStream('newer-large-file.txt');

readableStream.on('data', (chunk) => {
    console.log('====================== READING CHUNKS ========================');
    writableStream.write(chunk);
});

'use strict'

const fs = require('fs');
const Jimp = require('jimp');
const { Readable } = require('stream');

let theStream = fs.createReadStream('img.png');
//theStream.pipe(process.stdout);

// let myBuffer = [];
// theStream
//     .on('data', (chunk) => {
//         console.log(chunk);
//         console.log(chunk.toString());
//         //myBuffer = Buffer.concat(new Array(chunk));
//         myBuffer.push(Buffer.from(chunk.toString()));
//         //theStream.destroy();
//     })
//     .on('end', () => {
//         console.log('Data has been read');
//         myBuffer = Buffer.concat(myBuffer);
//         console.log(myBuffer);
//         console.log(myBuffer.toString());
//     }) // if .destroy() is invoked this line will be skipped
//     .on('close', () => console.log('Stream has been destroyed and file has been closed'));

printData();

// async function printData() {
//     const myData = await getBuffer(theStream);
//     console.log('The data is: ' + myData);
//     const readable = Readable.from(myData)
//     readable.pipe(process.stdout);
// }

// function getBuffer(theStream) {
//     let arrayBuffer = [];

//     return new Promise((resolve, reject) => {
//         theStream
//             .on('data', (chunk) => arrayBuffer.push(chunk))
//             .on('end', () => {
//                 console.log('Data has been read');
//                 resolve(arrayBuffer);
//             })
//             .on('error', (error) => {
//                 console.log('Failed to write into the buffer');
//                 reject(error.message);
//             });
//     });
// }

async function printData() {
    const resizedImage = await resizeImage(theStream);
    const myData = await getBuffer(resizedImage);
    console.log('The data is: ' + myData);
    const readable = new Readable();
    readable.push(myData);
    readable.push(null);
    readable.pipe(process.stdout);
}

function getBuffer(image) {
    let arrayBuffer = [];

    return new Promise((resolve, reject) => {
        image
            .on('data', (chunk) => arrayBuffer.push(chunk))
            .on('end', () => {
                console.log('Data has been read');
                resolve(Buffer.concat(arrayBuffer));
            })
            .on('error', (error) => {
                console.log('Failed to write into the buffer');
                reject(error);
            });
    });
}

async function resizeImage(imageStream) {
    let theImage = await getBuffer(imageStream);
    return Jimp
        .read(theImage)
        .then(image => setImageProperties(image))
        .catch(err => console.error(err));
}

function setImageProperties(image) {
    return image
        .resize(120, 120) // resize
        .write('img120x120.jpg'); // save
}

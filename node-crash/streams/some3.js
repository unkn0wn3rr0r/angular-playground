const fs = require('fs');

const readableStream = fs.createReadStream('img.png');
const writableStream = fs.createWriteStream('new-img.png');

let counter = 0;
readableStream.on('data', (chunk) => {
    console.log('====================== READING IMAGE CHUNKS ========================');
    writableStream.write(chunk);
    console.log(counter++);
});

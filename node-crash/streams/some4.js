const fs = require('fs');
const Jimp = require('jimp');
const { Readable } = require('stream');

const readableStream = fs.createReadStream('img.png');

// convert the stream to buffer so it can be able to be read and resized from Jimp
async function streamToBuffer(stream) {
    return new Promise((resolve, reject) => {
        const data = [];
        stream.on('data', (chunk) => data.push(chunk));
        stream.on('end', () => resolve(Buffer.concat(data)));
        stream.on('error', (error) => reject(error));
    });
}

// resize the image with Jimp
async function resizeImage(stream) {
    const buffer = await streamToBuffer(stream);
    return Jimp
        .read(buffer)
        .then(image => transformImage(image))
        .catch(error => console.log(error));
}

function transformImage(image) {
    return image
        .resize(100, 100)
        .write('resized-image.png');
}

async function convertResizedImageToReadableStream(stream) {
    const resizedImage = await resizeImage(stream);
    const readable = new Readable();
    readable.push(await resizedImage.getBufferAsync(Jimp.MIME_PNG));
    readable.push(null);
    return readable;
}

// get the resized image as a readable stream so we can pipe it to the response for the client
convertResizedImageToReadableStream(readableStream).then(data => {
    console.log('DATA READ SUCCESSFULLY');
    data.pipe(process.stdout);
});

// const person = require('./person.js');
// const Logger = require('./logger.js');

// const logger = new Logger();

// logger.on('message', data => console.log('Called listener', data));
// logger.log('Hello world..');

const http = require('http');
const fs = require('fs');
const path = require('path');

const server = http.createServer((req, res) => {
    if (req.url === '/') {
        res.writeHead(200, {
            'Content-Type': 'text/html'
        });
        res.end('<h1>Home</h1>');
    }
});

const PORT = process.env.PORT || 5000;

server.listen(PORT, () => console.log('Server running on port ' + PORT));
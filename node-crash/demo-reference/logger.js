const EventEmitter = require('events');
const uuid = require('uuid');

// Generate unique id
console.log(uuid.v4());
console.log(uuid.v4());
console.log(uuid.v4());
console.log(uuid.v4());

class Logger extends EventEmitter {

    log(message) {
        // call event
        this.emit('message', {
            id: uuid.v4(),
            message: message
        });
    }
}

module.exports = Logger;
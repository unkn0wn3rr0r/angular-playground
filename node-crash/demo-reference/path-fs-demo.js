'use strict';

const path = require('path');
const fs = require('fs');

// // Base file name
// console.log(__filename);
// // File name
// console.log(path.basename(__filename));
// // Create path obj
// console.log(path.parse(__filename));
// // Create new folder
// const folderName = '/test'
// fs.mkdir(path.join(__dirname, folderName), {}, err => {
//     if (err) {
//         console.log('Invalid folder name: ' + folderName);
//         throw err;
//     }
//     console.log('Folder created successfuly => ' + __dirname + folderName);
// });

const contactsPath = path.join(__dirname, 'contacts.json');
// const readContacts = fs.readFileSync(contactsPath).toString(); // or utf-8 instead as a second function parameter
// const contactsToJson = JSON.parse(readContacts);
// console.log(contactsToJson);

const newContact = [{
    "newContact": {
        "firstName": "Kaloyan",
        "lastName": "Stoyanov",
        "phoneNumber": "N/A",
    },
}]

const contact = stringify(newContact);
const saveNewContact = fs.writeFileSync(contactsPath, contact);
console.log(saveNewContact);

const readContacts = fs.readFileSync(contactsPath).toString(); // or utf-8 instead as a second function parameter
let contactsToJson = JSON.parse(readContacts);

contactsToJson.push({
    "newContact": {
        "firstName": "Kaloyan2",
        "lastName": "Stoyanov2",
        "phoneNumber": "N/A2",
    }
});

contactsToJson = stringify(contactsToJson);
fs.writeFileSync(contactsPath, contactsToJson);
console.log(contactsToJson);

function stringify(contact) {
    return JSON.stringify(contact, null, 2);
}
import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/models/Todo';
import { TodoService } from 'src/app/services/todo-service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todoList: Todo[];

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    this.todoService.getTodos().subscribe(data => this.todoList = data);
  }

  deleteTodo(theTodo: Todo) {
    this.todoList = this.todoList.filter(e => e !== theTodo);

    this.todoService.deleteTodo(theTodo).subscribe();
  }

  addTodo(theTodo: Todo): void {
    this.todoService.addTodo(theTodo).subscribe(data => this.todoList.push(data));
  }
}
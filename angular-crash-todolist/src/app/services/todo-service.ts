import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Todo } from '../../app/models/Todo';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  }),
}

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private todosUrl: string = 'https://jsonplaceholder.typicode.com/todos';
  private todosLimit = '?_limit=5'

  constructor(private httpClient: HttpClient) { }

  getTodos(): Observable<Todo[]> {
    return this.httpClient.get<Todo[]>(this.todosUrl + this.todosLimit);
  }

  toggleCompleted(theTodo: Todo): Observable<Object> {
    const currUrl = `${this.todosUrl}/${theTodo.id}`;

    return this.httpClient.put(currUrl, theTodo, httpOptions);
  }

  deleteTodo(theTodo: Todo): Observable<Todo> {
    const currUrl = `${this.todosUrl}/${theTodo.id}`;

    return this.httpClient.delete<Todo>(currUrl, httpOptions);
  }

  addTodo(theTodo: Todo): Observable<Todo> {
    return this.httpClient.post<Todo>(this.todosUrl, theTodo, httpOptions);
  }
}
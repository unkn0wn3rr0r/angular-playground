let count = 1;
(function waitForState() {
    setTimeout(() => {
        console.log(`Connection attempt: ${count++} readyState is: ${ws.readyState} ....`);
        if (ws.readyState === 1) {
            console.log('Connected!')
            return ws.send(data);
        }
        if (count > 1000) {
            throw new Error('Could not connect to WebSocket!');
        }
        waitForState();
    }, 0.00001);
}())

const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/chat.html');
});

//io.emit('some event', { someProperty: 'some value', otherProperty: 'other value' }); // This will emit the event to all connected sockets

const users = new Map();
let counter = 0;

io.on('connection', (socket) => {
    console.log('a user connected');
    users.set(socket.id, counter++);
    io.emit('message', `user ${users.get(socket.id)} connected...`);
    socket.on('chat message', (msg) => {
        io.emit('chat message', msg);
    });

    socket.on('disconnect', (socket) => {
        console.log('a user disconnected');
        io.emit('message', `user ${users.get(socket.id)} disconnected...`);
        users.delete(socket.id);
        console.log(socket);
    });
});

server.listen(3000, () => {
    console.log('listening on *:3000');
});

// Homework

// Here are some ideas to improve the application:

// Broadcast a message to connected users when someone connects or disconnects.
// Add support for nicknames.
// Don’t send the same message to the user that sent it. Instead, append the message directly as soon as he/she presses enter.
// Add “{user} is typing” functionality.
// Show who’s online.
// Add private messaging.

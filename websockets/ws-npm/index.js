const http = require('http');
const ws = require('ws');

const server = http.createServer();
const wss = new ws.WebSocketServer({ noServer: true });
const port = 8080;

let id = 0;
wss.on('connection', (client, req) => {
    id++
    console.log(`Client ${req.socket.address().address} connected`);
    client.on('message', (msg, bs) => {
        wss.clients.forEach((c) => {
            if (c != client && c.readyState === ws.WebSocket.OPEN) {
                c.send(msg, { binary: bs });
            }
        });
    });
    client.on('close', () => console.log(`Client ${id--} disconnected`));
    client.send('I am a message sent from the WebSocketServer');
});

server.on('upgrade', function upgrade(request, socket, head) {
    console.log('request =>', request.headers.cookie);

    if (request.headers.cookie) {
        wss.handleUpgrade(request, socket, head, function done(ws) {
            wss.emit('connection', ws, request, ws);
        });
    } else {
        console.log('Cookie not found...');
        socket.write('HTTP/1.1 401 Web Socket Protocol Handshake\r\n' +
            'Upgrade: WebSocket\r\n' +
            'Connection: Upgrade\r\n' +
            '\r\n');
    }
});

server.listen(port, () => console.log(`WebSocketServer listening on port ${port}`));

// wscat -c localhost:8080 --header cookie:12efsdfsd

const http = require('http');
const websocket = require('ws');

const server = http.createServer((req, res) => res.end("I am connected!"));
const wss = new websocket.Server({ server });
//wss.on('headers', (headers, req) => console.log(headers));

wss.on('connection', (ws, req) => {
    console.log(req.headers);
    ws.send('Server is now connected');
    ws.on('message', (message) => console.log(message));
    ws.on('close', () => console.log('Connection closed'));
});

server.listen(8000);

const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 8082 });

let clientCounter = 0;
wss.on('connection', ws => {
    console.log(`Client ${clientCounter++} connected..`);

    ws.on('message', data => {
        console.log(`Client says ${data}`);
    });

    ws.on('close', () => {
        console.log(`Client has disconnected`);
        clientCounter--;
    });
});

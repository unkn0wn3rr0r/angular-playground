var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
function ClassLogger(logInfo) {
    return function (constructor) { return console.log(logInfo); };
}
function PropertyLogger(target, name) {
    console.log(target, name, 'property logger');
}
function MethodLogger(target, name, property) {
    console.log(target, name, property, 'method logger');
}
function ParameterLogger(target, name, position) {
    console.log(target, name, position, 'parameter logger');
}
var Person = /** @class */ (function () {
    function Person(name) {
        this.name = name;
    }
    Person.prototype.getName = function () {
        return this.name;
    };
    Person.prototype.getFullname = function (lastName) {
        return this.name + ' ' + (lastName !== null && lastName !== void 0 ? lastName : 'No lastname');
    };
    __decorate([
        PropertyLogger
    ], Person.prototype, "name");
    __decorate([
        MethodLogger
    ], Person.prototype, "getName");
    __decorate([
        __param(0, ParameterLogger)
    ], Person.prototype, "getFullname");
    Person = __decorate([
        ClassLogger('Logging person...')
    ], Person);
    return Person;
}());
var p = new Person('Gosho');
console.log(p.getName);
console.log(p.getFullname());

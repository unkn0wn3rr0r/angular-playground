function ClassLogger(logInfo: string) {
    return (constructor: Function) => console.log(logInfo);
}
function PropertyLogger(target: unknown, name: string) {
    console.log(target, name, 'property logger');
}
function MethodLogger(target: unknown, name: string, property: PropertyDescriptor) {
    console.log(target, name, property, 'method logger');
}
function ParameterLogger(target: unknown, name: string, position: number) {
    console.log(target, name, position, 'parameter logger');
}
@ClassLogger('Logging person...')
class Person {
    @PropertyLogger
    name: string;
    constructor(name: string) {
        this.name = name;
    }
    @MethodLogger
    getName(): string {
        return this.name;
    }
    getFullname(@ParameterLogger lastName?: string): string {
        return this.name + ' ' + (lastName ?? 'No lastname');
    }
}
const p = new Person('Gosho');
console.log(p.getName);
console.log(p.getFullname());

function Required() {

}
function PositiveNumber() {

}
function validate<T>(param: T): T {
    return null;
}
class Course {
    title: string;
    price: number;
    constructor(title: string, price: number) {
        this.title = title;
        this.price = price;
    }
}
type ValidatorConfig = {
    [property: string]: {
        [validProperty: string]: string[]
    }
}
const registered: ValidatorConfig = {}
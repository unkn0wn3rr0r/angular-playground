type Admin = {
    name: string;
    privileges: string[];
}

type Employee = {
    name: string;
    startDate: Date
}

type CombinedType = Admin & Employee; // intersection between two types

const emp: CombinedType = {
    name: 'Kaloyan',
    privileges: ['the', 'bullshit'],
    startDate: new Date()
};

console.log(emp);

function add(a: CombinedType, b: CombinedType) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + b.toString();
    }
    return a.name + ' ' + b.name;;
}

console.log(
    add({
        name: 'Ivan',
        privileges: ['hmm'],
        startDate: new Date()
    },
        emp
    ));

type UnknownPerson = Employee | Admin;

function printInfo(unknownPerson: UnknownPerson) {
    if ('startDate' in unknownPerson) {
        console.log(unknownPerson.startDate);
    }
}

printInfo(emp);

interface Some {

    type: 'defaultTypeOne';
    name: string;
}

interface SomeTwo {

    type: 'defaultTypeTwo';
    name: string;
}

// indexed 
interface IndexedContainer {
    [prop: string]: string;
}

const someIndexedObject = {
    1: 'edno',
    2: 'dve',
    3: 'tri',
    4: 'chetiri'
};

console.log(someIndexedObject[1]);

"use strict";
const emp = {
    name: 'Kaloyan',
    privileges: ['the', 'bullshit'],
    startDate: new Date()
};
console.log(emp);
function add(a, b) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + b.toString();
    }
    return a.name + ' ' + b.name;
    ;
}
console.log(add({
    name: 'Ivan',
    privileges: ['hmm'],
    startDate: new Date()
}, emp));
function printInfo(unknownPerson) {
    if ('startDate' in unknownPerson) {
        console.log(unknownPerson.startDate);
    }
}
printInfo(emp);
const someIndexedObject = {
    1: 'edno',
    2: 'dve',
    3: 'tri',
    4: 'chetiri'
};
console.log(someIndexedObject[1]);

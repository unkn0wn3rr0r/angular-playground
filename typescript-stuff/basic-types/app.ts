const person: { name: string, age: number, hobbies: string[], role: [number, string] } = {
    name: 'Kaloyan',
    age: 27,
    hobbies: ['Street Workout', 'Fitness', 'Programming'],
    role: [2, 'author']
};

console.log(person.hobbies);
person.role[1] = 'asd';
console.log(person);

enum ResultType {
    asText, asNumber
}

type CombineType = number | string;
type ConverToType = 'asText' | 'asNumber';

const getType = {
    asText: (first: string, second: string) => first.toLocaleString() + second.toLocaleString(),
    asNumber: (first: number, second: number) => Number(first) + Number(second),
}

function combine(first: CombineType, second: CombineType, resultType: string) {
    if (resultType in getType === false) {
        return 'Invalid resultType: ' + resultType;
    }
    return getType[resultType](first, second);
}

console.log(combine(5, 5, ResultType[ResultType.asNumber]));
console.log(combine('Kaloyan ', 'Stoyanov', ResultType[ResultType.asText]));

function addNumbers(first: number, second: number, cb: (num: number) => number) {
    const sum = first + second;
    return cb(sum);
}

let sumNums = addNumbers(5, 6, function (sum) {
    return sum;
});

console.log(sumNums);

// function generateError(message: string, code: number): never {
//     throw {
//         message: message,
//         code: code
//     };
// }

// generateError('Not found', 404);

const copiedPerson = { ...person };

console.log(copiedPerson == person);
console.log(copiedPerson === person);

const add = (...a: number[]) => a;

const addedNumbers = add(1, 2, 3, 4, 5);

console.log(addedNumbers);

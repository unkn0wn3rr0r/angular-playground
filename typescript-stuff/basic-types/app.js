"use strict";
const person = {
    name: 'Kaloyan',
    age: 27,
    hobbies: ['Street Workout', 'Fitness', 'Programming'],
    role: [2, 'author']
};
console.log(person.hobbies);
person.role[1] = 'asd';
console.log(person);
var ResultType;
(function (ResultType) {
    ResultType[ResultType["asText"] = 0] = "asText";
    ResultType[ResultType["asNumber"] = 1] = "asNumber";
})(ResultType || (ResultType = {}));
const getType = {
    asText: (first, second) => first.toLocaleString() + second.toLocaleString(),
    asNumber: (first, second) => Number(first) + Number(second),
};
function combine(first, second, resultType) {
    if (resultType in getType === false) {
        return 'Invalid resultType: ' + resultType;
    }
    return getType[resultType](first, second);
}
console.log(combine(5, 5, ResultType[ResultType.asNumber]));
console.log(combine('Kaloyan ', 'Stoyanov', ResultType[ResultType.asText]));
function addNumbers(first, second, cb) {
    const sum = first + second;
    return cb(sum);
}
let sumNums = addNumbers(5, 6, function (sum) {
    return sum;
});
console.log(sumNums);
// function generateError(message: string, code: number): never {
//     throw {
//         message: message,
//         code: code
//     };
// }
// generateError('Not found', 404);
const copiedPerson = { ...person };
console.log(copiedPerson == person);
console.log(copiedPerson === person);
const add = (...a) => a;
const addedNumbers = add(1, 2, 3, 4, 5);
console.log(addedNumbers);

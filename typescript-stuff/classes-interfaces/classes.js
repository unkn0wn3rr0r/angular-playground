"use strict";
class Department {
    constructor(name) {
        this.name = name;
        this.name = name;
        this.employees = [];
    }
    describe() {
        console.log("Department " + this.name);
    }
    addEmployee(employee) {
        this.employees.push(employee);
    }
    printEmployeesInformation() {
        console.log(this.employees.length);
        console.log(this.employees);
    }
}
let ITDepartment = /** @class */ (() => {
    class ITDepartment extends Department {
        constructor(name, admins) {
            super(name);
            this.admins = admins;
            this.NO_ADMINS = 'No admins added yet';
            ITDepartment.employeeCounter++;
        }
        get getRootAdmin() {
            return this.admins.length < 1 ? this.NO_ADMINS : this.admins[0];
        }
        set setRootAdmin(admin) {
            if (!admin) {
                throw new Error('Invalid admin: ' + admin);
            }
            this.admins.unshift(admin);
        }
        // override base method
        printEmployeesInformation() {
            console.log(this.admins.length);
            console.log(this.admins);
        }
    }
    ITDepartment.employeeCounter = 0;
    return ITDepartment;
})();
class AccountingDepartment extends Department {
    // singleton
    constructor(name, reports) {
        super(name);
        this.reports = reports;
    }
    // singleton
    static getInstance(name, reports) {
        if (this.instance !== undefined && this.instance !== null) {
            return this.instance;
        }
        this.instance = new AccountingDepartment(name, reports);
        return this.instance;
    }
}
const accounting = AccountingDepartment.getInstance('Accounting', ['first', 'second', 'third', 'fourth']);
console.log(accounting);
// accounting2 returns the first instance ( above ), because of our singleton pattern used here to do so ( private constructor )
const accounting2 = AccountingDepartment.getInstance('Accounting II', ['1', '2', '3', '4']);
console.log(accounting2);
const itDepartment = new ITDepartment('IT', ['Johnny', "Frank", "Steve"]);
itDepartment.addEmployee('John');
itDepartment.addEmployee('Frank');
itDepartment.printEmployeesInformation();
itDepartment.describe();
console.log('Root admin is: ' + itDepartment.getRootAdmin);
itDepartment.setRootAdmin = 'Kaloyan';
console.log('Root admin is: ' + itDepartment.getRootAdmin);
itDepartment.printEmployeesInformation();
console.log(ITDepartment.employeeCounter);
// const copiedAcc = {
//     name: 'az',
//     describe: accounting.describe, // or bind it here => accounting.describe.bind(accounting)
// };
// copiedAcc.describe();

interface Person {

    name: string;
    age: number;

    getName(): string;
}

class John implements Person {

    name: string;
    age: number = 0;

    constructor(name: string) {
        this.name = name;
    }

    getName(): string {
        return this.name;
    }
}

interface Person {

    getAge?(): number;
}

let user1: Person = {
    name: 'Kaloyan',
    age: 27,
    getName(): string {
        return this.name;
    },
}

console.log(user1);
console.log(user1.name);
console.log(user1.age);
console.log(user1.getName());
console.log(user1.getAge?.() ?? 'No getAge method provided');

type PersonType = {

    name: string;
    age: number;

    getName(): string;
}

let user2: PersonType = {
    name: 'Kaloyan',
    age: 27,
    getName(): string {
        return this.name;
    },
}

// console.log(user2);
// console.log(user2.name);
// console.log(user2.age);
// console.log(user2.getName());

interface HasPhoneNumber {
    name: string;
    phone: number;
}
interface HasEmail {
    name: string;
    email: string;
}

let hasName: HasPhoneNumber | HasEmail = true ? { name: 'Mike', phone: 08888888888 } : { name: 'John', email: 'john@yahoo.com' };
console.log(hasName.name);

let hasAll: HasPhoneNumber & HasEmail = { name: 'Mike', phone: 3598888888888, email: 'mike@yahoo.com' };
console.log(hasAll.name, hasAll.phone, hasAll.email);

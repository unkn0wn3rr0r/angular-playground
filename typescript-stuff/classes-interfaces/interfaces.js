"use strict";
class John {
    constructor(name) {
        this.age = 0;
        this.name = name;
    }
    getName() {
        return this.name;
    }
}
let user1 = {
    name: 'Kaloyan',
    age: 27,
    getName() {
        return this.name;
    },
};
console.log(user1);
console.log(user1.name);
console.log(user1.age);
console.log(user1.getName());
console.log(user1.getAge?.() ?? 'No getAge method provided');
let user2 = {
    name: 'Kaloyan',
    age: 27,
    getName() {
        return this.name;
    },
};
let hasName = true ? { name: 'Mike', phone: 08888888888 } : { name: 'John', email: 'john@yahoo.com' };
console.log(hasName.name);
let hasAll = { name: 'Mike', phone: 3598888888888, email: 'mike@yahoo.com' };
console.log(hasAll.name, hasAll.phone, hasAll.email);

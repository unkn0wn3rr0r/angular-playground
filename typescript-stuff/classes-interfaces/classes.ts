class Department {

    private employees: string[];

    constructor(private name: string) {
        this.name = name;
        this.employees = [];
    }

    describe(this: Department): void { // 'this: Department' argument here is crucial when we are binding the describe in copiedAcc
        console.log("Department " + this.name);
    }

    addEmployee(employee: string): void {
        this.employees.push(employee);
    }

    printEmployeesInformation(): void {
        console.log(this.employees.length);
        console.log(this.employees);
    }
}

class ITDepartment extends Department {

    static employeeCounter = 0;

    private readonly NO_ADMINS = 'No admins added yet';

    constructor(name: string, public admins: string[]) {
        super(name);
        ITDepartment.employeeCounter++;
    }

    get getRootAdmin(): string {
        return this.admins.length < 1 ? this.NO_ADMINS : this.admins[0];
    }

    set setRootAdmin(admin: string) {
        if (!admin) {
            throw new Error('Invalid admin: ' + admin);
        }
        this.admins.unshift(admin);
    }

    // override base method
    printEmployeesInformation(): void {
        console.log(this.admins.length);
        console.log(this.admins);
    }
}

class AccountingDepartment extends Department {

    // singleton
    private static instance: AccountingDepartment;

    // singleton
    private constructor(name: string, public reports: string[]) {
        super(name);
    }

    // singleton
    static getInstance(name: string, reports: string[]) {
        if (this.instance !== undefined && this.instance !== null) {
            return this.instance;
        }
        this.instance = new AccountingDepartment(name, reports);
        return this.instance;
    }
}

const accounting = AccountingDepartment.getInstance('Accounting', ['first', 'second', 'third', 'fourth']);
console.log(accounting);
// accounting2 returns the first instance ( above ), because of our singleton pattern used here to do so ( private constructor )
const accounting2 = AccountingDepartment.getInstance('Accounting II', ['1', '2', '3', '4']);
console.log(accounting2);

const itDepartment = new ITDepartment('IT', ['Johnny', "Frank", "Steve"]);

itDepartment.addEmployee('John');
itDepartment.addEmployee('Frank');
itDepartment.printEmployeesInformation();
itDepartment.describe();
console.log('Root admin is: ' + itDepartment.getRootAdmin);
itDepartment.setRootAdmin = 'Kaloyan';
console.log('Root admin is: ' + itDepartment.getRootAdmin);
itDepartment.printEmployeesInformation();

console.log(ITDepartment.employeeCounter);


// const copiedAcc = {
//     name: 'az',
//     describe: accounting.describe, // or bind it here => accounting.describe.bind(accounting)
// };

// copiedAcc.describe();
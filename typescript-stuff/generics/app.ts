function merge<T extends object, U extends object>(objA: T, objB: U): T & U {
    return Object.assign(objA, objB);
}

const objA = {
    name: 'Kaloyan',
    age: 24
}

const objB = {
    age: 27,
    aka: 'unknown_error'
}

console.log(objA);
console.log(objB);

const mergedObj = merge(objA, objB);

console.log(mergedObj);

type Lengthy = {

    length: number;
}

function countAndDescribe<T extends Lengthy>(element: T): [T, string] {
    let description = 'Got no value';

    if (element.length === 1) {
        description = 'Got 1 element';
    } else if (element.length > 1) {
        description = 'Got ' + element.length + ' elements';
    }

    return [element, description];
}

console.log(countAndDescribe('a'));
console.log(countAndDescribe([]));
console.log(countAndDescribe([1, 2, 3]));

// generic class
class DataStorage<T> {

    private data: T[] = [];

    addItem(data: T): void {
        this.data.push(data);
    }

    removeItem(data: T): void {
        this.data.splice(this.data.indexOf(data), 1);
    }

    getItems(): T[] {
        return [...this.data];
    }
}

const textStorage = new DataStorage<string>();
textStorage.addItem('1, 2, 3 hmm hmm hmm');
textStorage.addItem('1, 2, 3 hmm');
textStorage.addItem('1, 2, 3 hmm hmm');
textStorage.removeItem('1, 2, 3 hmm');

console.log(textStorage.getItems());

type CourseGoal = {
    title: string;
    description: string;
    completeUntil: Date;
}

// Partial<T> and Readonly<T> are built in utility types
function createCourseGoal(title: string, description: string, completeUntil: Date): CourseGoal {
    let courseGoal: Partial<CourseGoal> = {};

    courseGoal.title = title;
    courseGoal.description = description;
    courseGoal.completeUntil = completeUntil;

    return courseGoal as CourseGoal;
}

const myCourse = createCourseGoal('Understanding TypeScript', '2021 edition', new Date());
console.log(myCourse, new Date(0), Date.now());

let names: Readonly<string[]> = ['Kaloyan', 'Olivia'];
// names.push('Jennifer'); NA on readonly object
// names.pop('Jennifer');  NA on readonly object
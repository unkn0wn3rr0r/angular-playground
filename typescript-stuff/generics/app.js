"use strict";
function merge(objA, objB) {
    return Object.assign(objA, objB);
}
const objA = {
    name: 'Kaloyan',
    age: 24
};
const objB = {
    age: 27,
    aka: 'unknown_error'
};
console.log(objA);
console.log(objB);
const mergedObj = merge(objA, objB);
console.log(mergedObj);
function countAndDescribe(element) {
    let description = 'Got no value';
    if (element.length === 1) {
        description = 'Got 1 element';
    }
    else if (element.length > 1) {
        description = 'Got ' + element.length + ' elements';
    }
    return [element, description];
}
console.log(countAndDescribe('a'));
console.log(countAndDescribe([]));
console.log(countAndDescribe([1, 2, 3]));
// generic class
class DataStorage {
    constructor() {
        this.data = [];
    }
    addItem(data) {
        this.data.push(data);
    }
    removeItem(data) {
        this.data.splice(this.data.indexOf(data), 1);
    }
    getItems() {
        return [...this.data];
    }
}
const textStorage = new DataStorage();
textStorage.addItem('1, 2, 3 hmm hmm hmm');
textStorage.addItem('1, 2, 3 hmm');
textStorage.addItem('1, 2, 3 hmm hmm');
textStorage.removeItem('1, 2, 3 hmm');
console.log(textStorage.getItems());
// Partial<T> and Readonly<T> are built in utility types
function createCourseGoal(title, description, completeUntil) {
    let courseGoal = {};
    courseGoal.title = title;
    courseGoal.description = description;
    courseGoal.completeUntil = completeUntil;
    return courseGoal;
}
const myCourse = createCourseGoal('Understanding TypeScript', '2021 edition', new Date());
console.log(myCourse, new Date(0), Date.now());
let names = ['Kaloyan', 'Olivia'];
// names.push('Jennifer'); NA on readonly object
// names.pop('Jennifer');  NA on readonly object

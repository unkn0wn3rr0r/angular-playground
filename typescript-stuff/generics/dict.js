"use strict";
function mapDict(dict, fn) {
    const result = {};
    Object.keys(dict).forEach((key, idx) => {
        const value = dict[key];
        if (typeof value !== 'undefined') {
            result[key] = fn(value, idx);
        }
    });
    return result;
}
const dict = mapDict({ a: 1, b: 2, c: 3 }, (str) => ({ val: str }));
console.log(dict);

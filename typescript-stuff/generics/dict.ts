type Dict<T> = {
    [key: string]: T | undefined;
}

function mapDict<T, S>(dict: Dict<T>, fn: (arg: T, idx: number) => S): Dict<S> {
    const result: Dict<S> = {};

    Object.keys(dict).forEach((key, idx) => {
        const value = dict[key];
        if (typeof value !== 'undefined') {
            result[key] = fn(value, idx);
        }
    });

    return result;
}

const dict = mapDict(
    { a: 1, b: 2, c: 3 },
    (str) => ({ val: str })
);

console.log(dict);

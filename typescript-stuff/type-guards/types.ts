interface HasName {
    name: string;
}

interface HasEmail {
    email: string;
}

type HasNameAndEmail = HasName | HasEmail;

function hasNameAndEmail(param: any): param is HasNameAndEmail {
    return typeof param.name === 'string' && typeof param.email === 'string';
}

const kaloyan: HasNameAndEmail = {
    name: 'Kaloyan',
    email: 'kaloyan@gmail.com'
};

if (hasNameAndEmail(kaloyan)) {
    console.log(kaloyan.name, kaloyan.email);
} else {
    console.log('nope..');
}

function isDefined<T>(param: T | undefined): param is T {
    return param !== undefined;
}

let x;
let xx = 5;

console.log(isDefined(xx));


type EventualType<T> = T extends Promise<infer S> ? S : T; // if T is of type Promise<S> then extract S, otherwise give me T

let a: EventualType<Promise<number>> = 5;           // it gives me the number in the Promise
let b: EventualType<number[]> = [1, 2, 3, 4, 5];    // it gives me the array of numbers

console.log(a);
console.log(b);

// @ts-expect-error
let kk: number = 'string?';

type StringOrNumber = string | number;
const someNumber: StringOrNumber = 555;
const someString: StringOrNumber = 'just a random string';

console.log(someNumber, someString);

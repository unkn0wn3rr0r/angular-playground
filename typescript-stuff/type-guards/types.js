"use strict";
function hasNameAndEmail(param) {
    return typeof param.name === 'string' && typeof param.email === 'string';
}
const kaloyan = {
    name: 'Kaloyan',
    email: 'kaloyan@gmail.com'
};
if (hasNameAndEmail(kaloyan)) {
    console.log(kaloyan.name, kaloyan.email);
}
else {
    console.log('nope..');
}
function isDefined(param) {
    return param !== undefined;
}
let x;
let xx = 5;
console.log(isDefined(xx));
let a = 5; // it gives me the number in the Promise
let b = [1, 2, 3, 4, 5]; // it gives me the array of numbers
console.log(a);
console.log(b);
// @ts-expect-error
let kk = 'string?';
const someNumber = 555;
const someString = 'just a random string';
console.log(someNumber, someString);

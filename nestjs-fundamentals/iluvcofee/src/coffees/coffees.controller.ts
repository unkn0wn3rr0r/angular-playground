import { Body, Controller, Delete, Get, Param, Patch, Post, Query, SetMetadata, UsePipes, ValidationPipe } from '@nestjs/common';
import { ParseIntPipe } from '@nestjs/common/pipes/parse-int.pipe';
import { Protocol } from 'src/common/decorators/protocol.decorator';
import { Public } from 'src/common/decorators/public.decorator';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { CoffeesService } from './coffees.service';
import { CreateCoffeeDto } from './dto/create-coffee.dto';
import { UpdateCoffeeDto } from './dto/update-coffee.dto';

// we can use pipes globally, on controllers, methods or parameters

//@UsePipes(ValidationPipe)
@Controller('coffees')
export class CoffeesController {

    constructor(private readonly coffeeService: CoffeesService) { }

    // @Public()
    // @Get('/xxx')
    // getCircular() {
    //     //  CIRCULAR REFERENCE SOLUTION
    //     const circularReference = {
    //         otherData: 123,
    //         myself: null
    //     };

    //     circularReference.myself = circularReference;

    //     type CircularObjectType = object | Object | null;

    //     const getCircularReplacer = () => {
    //         const seen = new WeakSet();
    //         return (key: string, value: CircularObjectType) => findCircularObject(seen, value);
    //     };

    //     function findCircularObject(seen: WeakSet<Object | object>, value: CircularObjectType) {
    //         if (typeof value === "object" && value !== null) {
    //             if (seen.has(value)) {
    //                 return;
    //             }
    //             seen.add(value);
    //         }
    //         return value;
    //     }

    //     const spacingLevel = 1;
    //     const theJson = JSON.stringify(circularReference, getCircularReplacer(), spacingLevel);
    //     console.log(theJson);

    //     try {
    //         console.log(JSON.stringify(circularReference));
    //     } catch (error) {

    //         const errorBody = {
    //             errorType: error.name,
    //             errorMessage: error.message,
    //             errorObject: theJson,
    //         };

    //         return errorBody;
    //     }

    //     function isCyclic(obj) {
    //         var seenObjects = [];

    //         function detect(obj) {
    //             if (obj && typeof obj === 'object') {
    //                 if (seenObjects.indexOf(obj) !== -1) {
    //                     return true;
    //                 }
    //                 seenObjects.push(obj);
    //                 for (var key in obj) {
    //                     if (obj.hasOwnProperty(key) && detect(obj[key])) {
    //                         console.log(obj, 'cycle at ' + key);
    //                         return true;
    //                     }
    //                 }
    //             }
    //             return false;
    //         }

    //         return detect(obj);
    //     }

    //     console.log(isCyclic(circularReference));
    // }

    @Public()
    @Get()
    async findAll(@Protocol() protocol: string, @Query() paginationQuery: PaginationQueryDto) {
        console.log(protocol);

        //await new Promise(resolve => setTimeout(resolve, 3000));
        return this.coffeeService.findAll(paginationQuery);
    }

    @Public()
    @Get(':id')
    findOne(@Param('id', ParseIntPipe) id: number) {
        return this.coffeeService.findOne('' + id);
    }

    @Public()
    @Post()
    create(@Body() createCoffeeDto: CreateCoffeeDto) {
        console.log(createCoffeeDto instanceof CreateCoffeeDto);
        return this.coffeeService.create(createCoffeeDto);
    }

    @Patch(':id')
    partailUpdate(@Param('id') id: string, @Body() updateCoffeeDto: UpdateCoffeeDto) {
        return this.coffeeService.update(id, updateCoffeeDto);
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.coffeeService.remove(id);
    }
}

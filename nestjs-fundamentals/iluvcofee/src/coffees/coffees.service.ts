import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { Event } from '../events/entities/event.entity';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { CreateCoffeeDto } from './dto/create-coffee.dto';
import { Coffee } from './entities/coffee.entity';
import { Connection, isValidObjectId, Model } from 'mongoose';
import { UpdateCoffeeDto } from './dto/update-coffee.dto';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';

@Injectable()
export class CoffeesService {

    constructor(
        @InjectModel(Coffee.name) private readonly coffeeModel: Model<Coffee>,
        @InjectModel(Event.name) private readonly eventModel: Model<Event>,
        @InjectConnection() private readonly connection: Connection) { }

    findAll(paginationQuery: PaginationQueryDto): Coffee[] {
        const { limit, offset } = paginationQuery;
        return this.coffeeModel.find()
            .skip(offset)
            .limit(limit)
            .exec();
    }

    async findOne(id: string): Promise<Coffee> {
        // if (!isValidObjectId(id)) {
        //     throw new HttpException(`Invalid coffees id: ${id}`, HttpStatus.BAD_REQUEST);
        // }

        const theCoffee = await this.coffeeModel.findOne({ _id: id }).exec();

        if (!theCoffee) {
            throw new HttpException(`Coffees ${id} not found`, HttpStatus.NOT_FOUND);
        }

        return theCoffee;
    }

    create(createCoffeeDto: CreateCoffeeDto): Promise<CreateCoffeeDto> {
        const coffee = new this.coffeeModel(createCoffeeDto);
        return coffee.save();
    }

    async update(id: string, updateCoffeeDto: UpdateCoffeeDto): Promise<UpdateCoffeeDto> {
        const existingCoffee = await this.coffeeModel
            .findOneAndUpdate(
                { _id: id },
                { $set: updateCoffeeDto },
                { new: true })
            .exec();

        if (!existingCoffee) {
            throw new NotFoundException(`Coffee with id: ${id} was not found!`);
        }

        return existingCoffee;
    }

    async remove(id: string) {
        const coffeeToRemove = await this.findOne(id);
        return coffeeToRemove.remove();
    }

    async recommendCoffee(coffee: Coffee) {
        const session = await this.connection.startSession();

        session.startTransaction();

        try {
            coffee.recommendations++;

            const recommendEvent = new this.eventModel({
                name: 'recommend_coffee',
                type: 'coffee',
                payload: {
                    coffeeId: coffee.id
                }
            });

            await recommendEvent.save({ session });
            await coffee.save({ session });

            await session.commitTransaction();

        } catch (error) {
            await session.abortTransaction();
        } finally {
            session.endSession();
        }
    }
}

import { Test, TestingModule } from '@nestjs/testing';
import { Connection } from 'mongoose';
import { CoffeesService } from './coffees.service';
import { Coffee, CoffeeSchema } from './entities/coffee.entity';
import { MongooseModule } from '@nestjs/mongoose';
import { EventSchema } from 'src/events/entities/event.entity';

describe('CoffeesService', () => {
  let service: CoffeesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forFeature([
          {
            name: Coffee.name,
            schema: CoffeeSchema,
          },
          {
            name: Event.name,
            schema: EventSchema
          }
        ]),
      ],
      providers: [CoffeesService,
        { provide: Connection, useValue: {} },
      ],
    }).compile();

    service = module.get<CoffeesService>(CoffeesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findOne', () => {
    describe('when coffee with id exists', () => {
      it('should return the coffee object with id', async () => {
        const coffeeId = '1';
        const expectedCoffee = {};

        const coffee = await service.findOne(coffeeId);
        expect(coffee).toEqual(expectedCoffee);
      })

    })
  })
});

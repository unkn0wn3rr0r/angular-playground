import { Module } from '@nestjs/common';
import * as mongoose from 'mongoose';

@Module({
    providers: [
        {
            provide: 'DATABASE_CONNECTION',
            useFactory: (): Promise<typeof mongoose> => mongoose.connect('mongodb://localhost/nest'),
        },
    ],
})
export class DatabaseModule { }

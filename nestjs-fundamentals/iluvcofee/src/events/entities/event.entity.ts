import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import * as mongoose from "mongoose";

@Schema()
export class Event extends mongoose.Document {

    @Prop()
    type: string;

    @Prop()
    name: string;

    //@Prop(mongoose.SchemaTypes.Mixed) // use this as a last resort, try to stick with SOME type not ANY type, so we don't need to use SchemaTypes.Mixed ( bad practice )
    payload: Record<string, any>
}

export const EventSchema = SchemaFactory.createForClass(Event);

import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class ParseIntPipe implements PipeTransform {

  transform(value: string, metadata: ArgumentMetadata) {
    const theInteger = Number.parseInt(value, 10);

    if (isNaN(theInteger)) {
      throw new BadRequestException(`Validation failed. ${theInteger} is not an integer.`);
    }

    return theInteger;
  }
}
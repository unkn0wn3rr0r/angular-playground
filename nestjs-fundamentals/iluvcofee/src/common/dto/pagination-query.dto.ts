import { Type } from "class-transformer";
import { IsOptional, IsPositive } from "class-validator";

export class PaginationQueryDto {

    @IsOptional()
    @IsPositive()
    // @Type(() => Number) -> this decorator manually or in main.ts => transformOptions: { enableImplicitConversion: true }, this will remove the need from setting @Type decorator
    limit: number;

    @IsOptional()
    @IsPositive()
    //@Type(() => Number)
    offset: number;
}
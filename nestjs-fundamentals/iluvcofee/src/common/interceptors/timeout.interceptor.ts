import { CallHandler, ExecutionContext, Injectable, NestInterceptor, RequestTimeoutException } from '@nestjs/common';
import { Observable, throwError, TimeoutError } from 'rxjs';
import { catchError, timeout } from 'rxjs/operators';

@Injectable()
export class TimeoutInterceptor implements NestInterceptor {

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle()
      .pipe(timeout(3000), catchError(error => this.throwWhenErrorOccures(error)));
  }

  throwWhenErrorOccures(error: Error): Observable<never> {
    if (error instanceof TimeoutError) {
      return throwError(new RequestTimeoutException('So slooow...'));
    }
    return throwError(error);
  }
}

function fakeAjax(url, cb) {
    const fake_responses = {
        "file1": "The first text",
        "file2": "The middle text",
        "file3": "The last text"
    };

    const randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;
    console.log("Requesting: " + url);

    setTimeout(function () {
        cb(fake_responses[url]);
    }, randomDelay);
}

function output(text) {
    console.log(text);
}

function getFile(file) {
    return new Promise((resolve, reject) => fakeAjax(file, resolve));
}

const th1 = getFile('file1');
const th2 = getFile('file2');
const th3 = getFile('file3');

th1.then(output)
    .then(() => th2)
    .then(output)
    .then(() => th3)
    .then(output)
    .then(() => output('Complete'))
    .catch(error => output(error));

function fakeAjax(url, cb) {
    const fake_responses = {
        file1: "The first text",
        file2: "The middle text",
        file3: "The last text"
    };
    const randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;
    console.log("Requesting: " + url);
    setTimeout(() => cb(fake_responses[url]), randomDelay);
}

const output = text => console.log(text);

function* getFile(file) {
    yield new Promise(resolve => fakeAjax(file, resolve));
}

async function run() {
    output('Loading data...')
    output(await getFile('file1').next().value);
    output(await getFile('file2').next().value);
    output(await getFile('file3').next().value);
    output('Complete!');
}

run();

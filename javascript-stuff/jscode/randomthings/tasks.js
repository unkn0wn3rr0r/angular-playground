function subSum(arr, start, end) {
    if (Array.isArray(arr) === false || arr.some(x => Number.isInteger(x) === false)) return NaN;
    if (start < 0) start = 0;
    if (end >= arr.length) end = arr.length - 1;

    return arr.slice(start, end + 1).reduce((a, b) => a + b, 0);
}

console.log(subSum([10, 20, 30, 40, 50, 60], 3, 300));

const seq = [
    "0", "0", "0",
    "1", "1", "1",
    "0", "0", "0", "0", "0", "0",
    "1", "1", "1", "1", "1",
    "0",
    "1",
    "0", "0",
    "1",
    "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
    "1",
    "0", "0", "0", "0", "0", "0", "0", "0", "0",
];

function longestSequence(seq) {
    let stack = [];
    let seqLength = 0;

    for (let i = 0; i < seq.length; i++) {
        if (seq[i] === "0") seqLength++;
        if (seq[i] === "1") {
            if (stack.length > 0) {
                stack.push(Math.max(stack.pop(), seqLength));
                seqLength = 0;
            } else {
                stack.push(seqLength);
                seqLength = 0;
            }
        }
        if (i === seq.length - 1) stack.push(Math.max(stack.pop(), seqLength));
    }
    return "max sequence length is: " + stack.pop();
}

console.log(longestSequence(seq));

const faces = new Map();
faces.set('2', 2);
faces.set('3', 3);
faces.set('4', 4);
faces.set('5', 5);
faces.set('6', 6);
faces.set('7', 7);
faces.set('8', 8);
faces.set('9', 9);
faces.set('10', 10);
faces.set('J', 'J');
faces.set('Q', 'Q');
faces.set('K', 'K');
faces.set('A', 'A');

const suits = new Map();
suits.set('S', '\u2660');
suits.set('H', '\u2665');
suits.set('D', '\u2666');
suits.set('C', '\u2663');

class MyError extends Error {

    constructor(message) {
        super();
        this.message = message;
    }
}

class Card {

    constructor(face, suit) {
        if (faces.has(face) === false) {
            throw new MyError('Error, invalid face');
        }

        if (suits.has(suit) === false) {
            throw new MyError('Error, invalid suit');
        }

        this.face = faces.get(face);
        this.suit = suits.get(suit);
    }

    toString() {
        return this.face + this.suit;
    }
}

function playingCards(face, suit) {
    try {
        let card = new Card(face, suit);
        return card.toString();
    } catch (error) {
        return error.message;
    }
}

const result = playingCards('2', 'C');
console.log(result);

function printDeckOfCards(cards) {
    const result = [];

    for (let card of cards) {
        const faceSuit = card.split('');
        const face = faceSuit.length > 2 ? faceSuit[0] + faceSuit[1] : faceSuit[0];
        const suit = faceSuit.length > 2 ? faceSuit[2] : faceSuit[1];
        const newCard = playingCards(face, suit);

        if (newCard.includes('Error, ')) return 'Invalid card: ' + card;

        result.push(newCard);
    }

    return result.join(' ');
}

console.log(printDeckOfCards(['AS', '10D', 'KH', '2C']));
console.log(printDeckOfCards(['5S', '3D', 'QD', '1C']));
console.log(printDeckOfCards(['3D', 'JC', '2S', '10H', '5X']));


module.exports = {
    subSum: subSum,
    playingCards: playingCards
}

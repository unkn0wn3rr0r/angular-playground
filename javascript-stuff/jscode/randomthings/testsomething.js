let faces = new Map();
faces.set(2, 2);
faces.set(3, 3);
faces.set(4, 4);
faces.set(5, 5);
faces.set(6, 6);
faces.set(7, 7);
faces.set(8, 8);
faces.set(9, 9);
faces.set(10, 10);
faces.set('J', 'J');
faces.set('Q', 'Q');
faces.set('K', 'K');
faces.set('A', 'A');

if (faces.has('Qq')) {
    console.log(faces.get('Q'));
} else {
    console.log('key was not found');
}

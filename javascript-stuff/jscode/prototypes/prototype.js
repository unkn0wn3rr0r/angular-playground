'use strict';

// constructor function
function Person() {
    this.age = 324324;
    this.name = 'Kaloyan';
    this.greet = () => console.log(`Hello i am ${this.name} and i am ${this.age} years old.`);
}

Person.prototype.bye = function bye() {
    console.log(`Bye from ${this.name}`)
}

const person = new Person();
person.greet();
console.log(person.__proto__);
console.table(person);

function check(data) {
    return data.hasOwnProperty('age')
        && data.hasOwnProperty('greet')
        && data.hasOwnProperty('bye');
}

console.log(check(person));
person.bye();

const person2 = new person.__proto__.constructor();
console.log(person2);
console.log(check(person));
console.log(Object.is(person, person2));
console.log(Object.getPrototypeOf(person));

const someObj = Object.setPrototypeOf(person, {
    ...Object.getPrototypeOf(person),
    printSomething() {
        console.log('Something...');
    },
});

console.log(Object.getPrototypeOf(person));
console.log('Are equal? ->', someObj == person);
console.log(person);
person.printSomething();
console.log(someObj);

const kaloyanResults = [];
const steveResults = [];

for (let i = 0; i < 10; i++) {
    console.info('== ROUND', i + 1);

    // DATE
    console.log('== DATE API BENCHMARK');
    const start1 = new Date();
    const staticArr1 = new Array(10000000);
    for (let i = 0; i < 10000000; i++) {
        staticArr1[i] = { index: i, regions: ['us-east2', 'us-east1'] };
    }
    const end1 = new Date();
    const result1 = end1.getTime() - start1.getTime();
    console.log('kaloyan', result1);

    const start2 = new Date();
    const staticArr2 = staticArr1.map((data) => {
        return { index: data.index, regions: data.regions };
    });
    const end2 = new Date();
    const result11 = end2.getTime() - start2.getTime();
    console.log('steve', result11);


    // PERFORMANCE
    console.log('== PERFORMANCE API BENCHMARK');
    const { performance } = require('perf_hooks');

    const timeStart1 = performance.now();
    const staticArr11 = new Array(10000000);
    for (let i = 0; i < 10000000; i++) {
        staticArr11[i] = { index: i, regions: ['us-east2', 'us-east1'] };
    }
    const timeEnd1 = performance.now();
    const result2 = timeEnd1 - timeStart1;
    console.log('kaloyan', result2);

    const timeStart2 = performance.now();
    const staticArr22 = staticArr11.map((data) => {
        return { index: data.index, regions: data.regions };
    });
    const timeEnd2 = performance.now();
    const result22 = timeEnd2 - timeStart2;
    console.log('steve', result22);

    kaloyanResults.push(result1, result2);
    steveResults.push(result11, result22);


    // CONSOLE.TIME
    console.log('== CONSOLE API BENCHMARK');
    console.time('kaloyan');
    const staticArr111 = new Array(10000000);
    for (let i = 0; i < 10000000; i++) {
        staticArr111[i] = { index: i, regions: ['us-east2', 'us-east1'] };
    }
    console.timeEnd('kaloyan');

    console.time('steve');
    const staticArr222 = staticArr111.map((data) => {
        return { index: data.index, regions: data.regions };
    });
    console.timeEnd('steve');
}

const resultK = kaloyanResults.reduce((a, b) => a + b, 0);
const resultS = steveResults.reduce((a, b) => a + b, 0)
console.log('kaloyan final =>', resultK);
console.log('steve   final =>', resultS);

if (Math.max(resultK, resultS) === resultK) {
    console.log('Kaloyan sorry hermano, but you lost..');
} else {
    console.log('Steve sorry hermano, but you lost..');
}

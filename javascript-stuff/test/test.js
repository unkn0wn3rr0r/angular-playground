const { expect } = require('chai');
const { subSum, playingCards } = require('../tasks');

describe('Sub sum task', () => {
    it('Should sum numbers from array in a given range from start to end', () => {
        expect(typeof subSum).to.be.equal("function");
    });

    it('The sum should be equal to a given number', () => {
        expect(subSum([10, 20, 30, 40, 50, 60], 3, 300)).to.be.equal(150);
    });

    it('Should be invalid array', () => {
        expect(subSum(10, 20, "asdasd", 40, 50, 60, 0, 300)).to.not.be.equal(NaN);
    });

    it('Should be invalid array', () => {
        expect(subSum([10, 20, "asdasdad", 40, 50, 60], 3, 300)).to.not.be.equal(NaN);
    });

    it('When empty array the result should be 0', () => {
        expect(subSum([], 1, 2)).to.be.equal(0);
    });

    it('Should be invalid array', () => {
        expect(subSum(["text"], 0, 2)).to.not.be.equal(NaN);
    });
});

describe('Playing cards task', () => {
    it('Should return correct pair: 2, C => 2♣', () => {
        expect(playingCards('2', 'C')).to.be.equal('2♣');
    });

    it('Should return correct pair: 10, H => 10♥', () => {
        expect(playingCards('10', 'H')).to.be.equal('10♥');
    });

    it('Should return incorrect pair: 1, C => Error, invalid face', () => {
        expect(playingCards('1', 'C')).to.be.equal('Error, invalid face');
    });

    it('Should return incorrect pair: 2, G => Error, invalid suit', () => {
        expect(playingCards('2', 'G')).to.be.equal('Error, invalid suit');
    });
});

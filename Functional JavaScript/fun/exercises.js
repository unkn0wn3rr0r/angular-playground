const doubl = n => n * 2;
const square = n => n * n;
const sub = (a, b) => a - b;
const mul = (a, b) => a * b;
const add = (a, b) => a + b;
const addf = n => m => n + m;
const identityf = param => () => param;
const liftf = func => a => b => func(a, b);
const curry = (func, a) => b => func(a, b);
const composeu = (fun1, fun2) => (x) => fun2(fun1(x));
const reverse = func => (...args) => func(...args.reverse());
const composeb = (fun1, fun2) => (a, b, c) => fun2(c, fun1(a, b));

const limit = (binary, count) => (a, b) => {
    if (count >= 1) {
        count--;
        return binary(a, b);
    }
    return undefined;
}

let three = identityf(333);
console.log(three());
console.log(addf(3)(4));

let mult = liftf(mul)(5)(6);
console.log(mult);

let addition = liftf(add)(3)(4);
console.log(addition);

let add3 = curry(add, 4)(3);
console.log('curry + : ' + add3);

let mul6 = curry(mul, 5)(6);
console.log('curry * : ' + mul6);

let inc5 = curry(add, 5);
console.log(inc5(1));

let inc6 = curry(add, 6);
console.log(inc6(1));

const bus = reverse(sub);
console.log(bus(4, 3));

const composedu = composeu(doubl, square)(5);
console.log(composedu);

const composedb = composeb(add, mul)(2, 3, 7);
console.log(composedb);

const limitfun = limit(add, 2);
console.log(limitfun(3, 4));
console.log(limitfun(3, 5));
console.log(limitfun(3, 5));

const add = (x, y) => x + y;
const toPair = f => (x, y) => f(x, y);
const curry = f => x => y => f(x, y);
const result = toPair(add)(5, 6);
const result2 = curry(add)(5)(6);

console.log(result);
console.log(result2);

const modulo = curry((x, y) => y % x);
const isOdd = modulo(2);

console.log(isOdd(33));
console.log(isOdd(22));
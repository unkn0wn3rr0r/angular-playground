const toUpperCase = str => str.toUpperCase();
const exclaim = str => str + '!';
const compose = (f, g) => x => f(g(x));
const shout = compose(exclaim, toUpperCase);

console.log(shout('ou em ge'));
const Box = x => ({
    map: f => Box(f(x)),
    fold: f => f(x),
    inspect: `Box(${x})`,
})

const fromStringToNumber = str =>
    Box(str)
        .map(str => str.trim())
        .map(trimmed => parseInt(trimmed))
        .map(number => new Number(number + 1))
        .fold(String.fromCharCode);

const result = fromStringToNumber('     64  ');
console.log(result);